var scroll = new SmoothScroll('a[href*="#"]', {
    // Selectors
    header: '.header',
    ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
    topOnEmptyHash: false, // Scroll to the top of the page for links with href="#"
});

if ( window.location.hash ) {
    var anchor = document.querySelector( window.location.hash ); // Get the anchor
    var options = {
        header: '.header',
	    ignore: '[data-scroll-ignore]', // Selector for links to ignore (must be a valid CSS selector)
	    topOnEmptyHash: false, // Scroll to the top of the page for links with href="#"
    }; // Any custom options you want to use would go here
    var scrollCustom = new SmoothScroll();
    // waiting loading animation
    setTimeout(function() {
    	scrollCustom.animateScroll( anchor, false, options );
    }, 1500);
}